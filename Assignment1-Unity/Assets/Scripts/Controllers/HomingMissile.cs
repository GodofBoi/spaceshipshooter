﻿using UnityEngine;
using System.Collections;

public class HomingMissile : MonoBehaviour
{
    public float ForwardSpeed = 1;
    public float RotateSpeedInDeg = 45;

    // In Update, you should rotate and move the missile to rotate it towards the player.  It should move forward with ForwardSpeed and rotate at RotateSpeedInDeg.
    // Do not use the RotateTowards or LookAt methods.
    void Update()
    {
        //float missleSpeed = this.transform.position.x + ForwardSpeed * Time.deltaTime;

        //Vector3 misslePosition = new Vector3(missleSpeed, this.transform.position.y, this.transform.position.z);

        Vector3 enemyPosition = GameController.GetEnemyObject().transform.position;

        Vector3 vector_1 = enemyPosition - this.transform.position;

        float dotProduct = Vector3.Dot(vector_1.normalized, transform.right);

        float leftOrRight = 0;

        if(dotProduct >= 0)
        {
            leftOrRight = -1;
        }
        if(dotProduct < 0)
        {
            leftOrRight = 1;
        }

        transform.Translate(Vector3.up * ForwardSpeed * Time.deltaTime);

        transform.Rotate(Vector3.forward, leftOrRight * RotateSpeedInDeg * Time.deltaTime);

    }
}
