﻿using UnityEngine;
using System.Collections;

public class ShipMotor : MonoBehaviour
{
    public float AccelerationTime = 1;
    public float DecelerationTime = 1;
    public float MaxSpeed = 1;
    Vector3 DirectionVec;
    public float speed;

    /// <summary>
    /// Move the ship using it's transform only based on the current input vector. Do not use rigid bodies.
    /// </summary>
    /// <param name="input">The input from the player. The possible range of values for x and y are from -1 to 1.</param>
    public void HandleMovementInput( Vector2 input )
    {
        //check for input
        if(input.sqrMagnitude > 0)
        {

            DirectionVec = input.normalized;

            speed = speed + MaxSpeed * Time.deltaTime / AccelerationTime;
        }

        //Check for no input
        if(input.sqrMagnitude == 0)
        {
            speed = speed - MaxSpeed * Time.deltaTime / DecelerationTime;
        }

        //Limit speed
        speed = Mathf.Clamp(speed, 0, MaxSpeed);

        transform.position = transform.position + DirectionVec * speed * Time.deltaTime;

        ////Controls the speed of the ship
        //float shipSpeed = 0;

        ////float speedCap = Mathf.Clamp(MaxSpeed, 0, 1);

        ////Acceleration of the ship
        //float acceleration = MaxSpeed / AccelerationTime;

        ////Deceleration of the ship
        //float deceleration = MaxSpeed / DecelerationTime;



        ////MaxSpeed = MaxSpeed - DecelerationTime * Time.deltaTime;

        //if(input.y == 0 && input.x == 0)
        //{
        //    //shipSpeed = deceleration * DecelerationTime;
        //}

        ////Go up
        //if (input.y > 0)
        //{
        //    shipSpeed = acceleration * Time.deltaTime;
        //    shipSpeed = shipSpeed + acceleration * AccelerationTime;
        //    transform.Translate(0, shipSpeed, 0);
        //    print(shipSpeed);
        //}

        ////Go down
        //if (input.y < 0)
        //{
        //    shipSpeed = shipSpeed - acceleration * Time.deltaTime;
        //    transform.Translate(0, shipSpeed, 0);
        //}

        ////Go right
        //if(input.x > 0)
        //{
        //    //transform.Translate(MaxSpeed = MaxSpeed + AccelerationTime * Time.deltaTime, 0, 0);
        //}

        ////Go left
        //if(input.x < 0)
        //{
        //    //transform.Translate(MaxSpeed = MaxSpeed - AccelerationTime * Time.deltaTime, 0, 0);
        //}

        //if(shipSpeed >= MaxSpeed)
        //{
        //    shipSpeed = MaxSpeed;
        //}

    }
    
}
