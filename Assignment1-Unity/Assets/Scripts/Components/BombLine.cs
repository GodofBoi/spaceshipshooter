﻿using UnityEngine;
using System.Collections;

public class BombLine : MonoBehaviour
{
    public GameObject [] amountOfBombs;
    public GameObject BombPrefab;
    public int BombCount;
    public float BombSpacing;

    /// <summary>
    /// Spawn a line of instantiated BombPrefabs behind the player ship. There should be BombCount bombs placed with BombSpacing amount of space between them.
    /// </summary>
    /// <returns>An array containing all the bomb objects</returns>
    public GameObject[] SpawnBombs()
    {

        for (int i = 0; i < BombCount; i++)
        {
            BombSpacing = BombSpacing - 1f;
            Instantiate(BombPrefab, this.transform.position + (-this.transform.up * -BombSpacing), Quaternion.identity);
        }
        BombSpacing = 1f;

        //foreach(GameObject bombCount_g in amountOfBombs)
        //{
        //    Instantiate(BombPrefab, Player.playerPosition * BombSpacing, Quaternion.identity);
        //}

        return amountOfBombs;
    }
    
}
