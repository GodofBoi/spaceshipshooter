﻿using UnityEngine;
using System.Collections;

public class VectorToEnemy : MonoBehaviour
{
    private float vectorLengthX_l;
    private float vectorLengthY_l;
    private float vectorLenghtZ_l;
    private float vectorDistanceX;
    private float vectorDistanceY;
    [SerializeField]
    private float vectorLengthTotal;
    [SerializeField]
    private Vector3 vectorLength;
    [SerializeField]
    private float totalDistance;

    /// <summary>
    /// Calculated vector from the player to enemy found by GameManager.GetEnemyObject
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The vector from the player to the enemy.</returns>
    public Vector3 GetVectorToEnemy()
    {
        GameController.GetPlayerObject();

        vectorLengthX_l = (GameController.GetPlayerObject().transform.position.x + GameController.GetEnemyObject().transform.position.x);
        vectorLengthY_l = (GameController.GetPlayerObject().transform.position.y + GameController.GetEnemyObject().transform.position.y);
        vectorLenghtZ_l = (GameController.GetPlayerObject().transform.position.z + GameController.GetEnemyObject().transform.position.z);

        //vectorLengthTotal = Mathf.Sqrt(vectorLengthX * vectorLengthX + vectorLengthY * vectorLengthY + vectorLenghtZ * vectorLenghtZ);
        //Debug.Log(vectorLengthTotal);

        vectorLength = new Vector3(vectorLengthX_l, vectorLengthY_l, vectorLenghtZ_l);
        Debug.Log(vectorLength);

        return vectorLength;
    }

    /// <summary>
    /// Calculates the distance from the player to the enemy returned by GameManager.GetEnemyObject without using calls to magnitude.
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The scalar distance between the player and the enemy</returns>
    public float GetDistanceToEnemy()
    {
        vectorDistanceX = Mathf.Pow(2, GameController.GetPlayerObject().transform.position.x - GameController.GetEnemyObject().transform.position.x);
        vectorDistanceY = Mathf.Pow(2, GameController.GetPlayerObject().transform.position.y - GameController.GetEnemyObject().transform.position.y);

        totalDistance = Mathf.Sqrt(vectorDistanceX + vectorDistanceY + 0);
        Debug.Log(totalDistance);

        return totalDistance;
    }
    
}
