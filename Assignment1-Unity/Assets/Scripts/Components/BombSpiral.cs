﻿using UnityEngine;
using System.Collections;

public class BombSpiral : MonoBehaviour
{
    public GameObject BombPrefab;
    [Range(5, 25)]
    public float SpiralAngleInDegrees = 10;
    public int BombCount = 10;
    public float StartRadius = 1;
    public float EndRadius = 3;

    /// <summary>
    /// Spawns spirals of BombPrefab game objects around the player. Create BombCount number of bombs 
    /// around the player, with each bomb being spaced SpiralAngleInDegrees apart from the next. The spiral 
    /// starts at StartRadius away from the player and ends at EndRadius away from the player.
    /// </summary>
    /// <returns>An array of the spawned bombs</returns>
    public GameObject[] SpawnBombSpiral()
    {
        GameObject[] bombSprialArray = new GameObject[BombCount];

        for (int i = 0; i < bombSprialArray.Length; i++)
        {
            float bombRadius = Mathf.Lerp(StartRadius, EndRadius, (i + 1) / (float) BombCount);

            float X = Mathf.Cos(SpiralAngleInDegrees * Mathf.Deg2Rad * i);
            float Y = Mathf.Sin(SpiralAngleInDegrees * Mathf.Deg2Rad * i);

            Vector3 bombPosition = new Vector3(X, Y) * bombRadius;

            Vector3 playerPos = transform.position;

            Instantiate(BombPrefab, playerPos + bombPosition, Quaternion.identity);
        }

        return bombSprialArray;
    }

}
