﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour
{
    public GameObject PowerUpPrefab;
    public int PowerUpCount = 3;
    public float PowerUpRadius = 1;

    /// <summary>
    /// Spawn a circle of PowerUpCount power up prefabs stored in PowerUpPrefab, evenly spaced, around the player with a radius of PowerUpRadius
    /// </summary>
    /// <returns>An array of the spawned power ups, in counter clockwise order.</returns>
    public GameObject[] SpawnPowerUps()
    {
        //Creating new array for the powerups
        GameObject[] PowerUpArray = new GameObject[PowerUpCount];

        //for each powerup count spawn one powerup around the ship
        for (int i = 0; i < PowerUpArray.Length; i++)
        {
            float X = Mathf.Cos((i * 360 / PowerUpCount) * Mathf.Deg2Rad);
            float Y = Mathf.Sin((i * 360 / PowerUpCount) * Mathf.Deg2Rad);

            Vector3 powerUp = new Vector3(X, Y);

            Vector3 playerPos = transform.position;

            Instantiate(PowerUpPrefab, playerPos + powerUp, Quaternion.identity);
        }

        return PowerUpArray;
    }
}
