﻿using UnityEngine;
using System.Collections;

public class VisionCone : MonoBehaviour
{

    public float AngleSweepInDegrees = 60;
    public float ViewDistance = 3;

    /// <summary>
    /// Calculates whether the player is inside the vision cone of an enemy as defined by the AngleSweepIndegrees
    /// and ViewDistance varilables. Do not use any magnitude or Distance methods.  You may use any of the previous
    /// methods you have written.
    /// </summary>
    /// <see cref="GameController"/>
    /// <returns>Whether the player is within the enemy's vision cone.</returns>
    public bool IsPlayerInVisionCone()
    {
        //Bool for when the player is found
        bool playerFound;

        //find the player position
        Vector3 playerPosition = GameController.GetPlayerObject().transform.position;

        //
        float playerAngle = Mathf.Atan2(playerPosition.y - transform.position.y, playerPosition.x - transform.position.x) * Mathf.Rad2Deg;

        //Point on the right of the cone
        float rightCone = (Mathf.Atan2(transform.up.y, transform.up.x) * Mathf.Rad2Deg) - (AngleSweepInDegrees / 2);

        //Point on the left of the cone
        float leftCone = (Mathf.Atan2(transform.up.y, transform.up.x) * Mathf.Rad2Deg) + (AngleSweepInDegrees / 2);

        //If player angle is between the right and left cone, player is found
        if (playerAngle >= rightCone && playerAngle <= leftCone)
        {
            playerFound = true;
        }
        else
            playerFound = false;

        return playerFound;
    }
    
}
